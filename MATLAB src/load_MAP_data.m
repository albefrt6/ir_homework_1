%% Initialize variables.
filename_1 = '/home/alb/Desktop/University/IR/IR_HW1/Evaluations/1_StopwordsStemmerBM25_MAP.txt';
filename_2 = '/home/alb/Desktop/University/IR/IR_HW1/Evaluations/2_StopwordsStemmerTFIDF_MAP.txt';
filename_3 = '/home/alb/Desktop/University/IR/IR_HW1/Evaluations/3_NoStoplistStemmerBM25_MAP.txt';
filename_4 = '/home/alb/Desktop/University/IR/IR_HW1/Evaluations/4_NoStoplistNoStemmerTFIDF_MAP.txt';
delimiter = '\t';
startRow = 2;
endRow = 51;

%% Format for each line of text:
%   column3: double (%f)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%*s%*s%f%[^\n\r]';

%% Open the text file.
fileID_1 = fopen(filename_1,'r');
fileID_2 = fopen(filename_2,'r');
fileID_3 = fopen(filename_3,'r');
fileID_4 = fopen(filename_4,'r');


%% Read columns of data according to the format.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray_1 = textscan(fileID_1, formatSpec, endRow-startRow+1, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
dataArray_2 = textscan(fileID_2, formatSpec, endRow-startRow+1, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
dataArray_3 = textscan(fileID_3, formatSpec, endRow-startRow+1, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
dataArray_4 = textscan(fileID_4, formatSpec, endRow-startRow+1, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');

%% Close the text file.
fclose(fileID_1);
fclose(fileID_2);
fclose(fileID_3);
fclose(fileID_4);

%% Post processing for unimportable data.
% No unimportable data rules were applied during the import, so no post
% processing code is included. To generate code which works for
% unimportable data, select unimportable cells in a file and regenerate the
% script.

%% Allocate imported array to column variable names
%run_2 = dataArray{:, 1};
%dataArray_1 = cellfun(@(x) num2cell(x), dataArray_1, 'UniformOutput', false);
%dataArray_2 = cellfun(@(x) num2cell(x), dataArray_2, 'UniformOutput', false);
%dataArray_3 = cellfun(@(x) num2cell(x), dataArray_3, 'UniformOutput', false);
%dataArray_4 = cellfun(@(x) num2cell(x), dataArray_4, 'UniformOutput', false);

measure_MAP = [ dataArray_1{1:end-1} dataArray_2{1:end-1} dataArray_3{1:end-1} dataArray_4{1:end-1} ]
runID_MAP = [ "Stopwords&Stemmer BM25", "Stopwords&Stemmer TFIDF", "NoStoplist&Stemmer BM25", "NoStoplist&NoStemmer TFIDF" ]
topicID_MAP = 351 : 400


%% Clear temporary variables
clearvars filename_1 filename_2 filename_3 filename_4 delimiter startRow endRow formatSpec fileID_1 fileID_2 fileID_3 fileID_4 dataArray_1 dataArray_2 dataArray_3 dataArray_4 ans;