# IR_Homework_1
Homework 1 per il corso di Reperimento dell'Informazione del Corso di Laurea Magistrale in Ingegneria Informatica.
Lo scopo del lavoro è eseguire diverse run con un sistema di IR, valutarle e condurre il test statistico ANOVA 1-way, plottando i relativi risultati. La collezione usata è la _TREC COLLECTION (TIPSTER Disk 4 & 5, senza Congressional Records)_.


## Tool
- **Terrier**: sistema di indicizzazione e reperimento
- **Trec_eval**: valutazione dei risultati
- **Bash**: scripting per l'automazione delle run
- **MATLAB**: plotting dei risultati
