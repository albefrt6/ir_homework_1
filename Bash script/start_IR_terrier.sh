COLLECTION_FOLDER="/home/alb/Desktop/University/IR/IR_HW1/TREC7"
TOPICS_FILE="/home/alb/Desktop/University/IR/IR_HW1/topics.351-400_trec7.txt"
POOL_FILE="/home/alb/Desktop/University/IR/IR_HW1/qrels.trec7.txt"
RESULTS_FNAME="/home/alb/Desktop/University/IR/IR_HW1/Evaluations/"

TERRIER_FOLDER="/home/alb/Desktop/University/IR/IR_HW1/terrier-core-4.4/"
TREC_EVAL_FOLDER="/home/alb/Desktop/University/IR/IR_HW1/terrier-core-4.4/bin"
RESULTS_FILE="/home/alb/Desktop/University/IR/IR_HW1/terrier-core-4.4/var/"
stopwords_enabled=0

cd $TERRIER_FOLDER

#get terrier setup for using a trec collection
bin/trec_setup.sh $COLLECTION_FOLDER

#rebuild the collection.spec file correctly
#find $COLLECTION_FOLDER -type f | sort |grep -v info > etc/collection.spec

#use this file for the topics
echo trec.topics=$TOPICS_FILE >> etc/terrier.properties

#use this file for query relevance assessments
echo trec.qrels=$POOL_FILE >> etc/terrier.properties

#removing default parameters
sed -i 's/termpipelines=Stopwords,PorterStemmer//' $TERRIER_FOLDER/etc/terrier.properties
sed -i 's/stopwords.filename=stopword-list.txt//' $TERRIER_FOLDER/etc/terrier.properties
echo termpipelines= >> etc/terrier.properties

#setting different parameters
sed -i 's/TrecQueryTags.process=TOP,NUM,TITLE/TrecQueryTags.process=TITLE,DESC/' $TERRIER_FOLDER/etc/terrier.properties
sed -i 's/TrecQueryTags.skip=DESC,NARR/TrecQueryTags.skip=NARR/' $TERRIER_FOLDER/etc/terrier.properties

read -p "Add Stoplist? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo stopwords.filename=stopword-list.txt >> etc/terrier.properties
    sed -i 's/termpipelines=/termpipelines=Stopwords/' $TERRIER_FOLDER/etc/terrier.properties
    stopwords_enabled=1
    #[[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi

read -p "Add PorterStemmer? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    if [[($stopwords_enabled == 1)]]
    then
      sed -i 's/termpipelines=Stopwords/termpipelines=Stopwords,PorterStemmer/' $TERRIER_FOLDER/etc/terrier.properties
    else
      sed -i 's/termpipelines=/termpipelines=PorterStemmer/' $TERRIER_FOLDER/etc/terrier.properties
    fi

    #echo termpipelines=Stopwords >> etc/terrier.properties
    #[[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi

#index the collection
echo Folder name of the index?
read index_folder
echo terrier.index.path=$index_folder/ >> etc/terrier.properties
temp="_run"
mkdir var/$index_folder
echo trec.results=$index_folder$temp/ >> etc/terrier.properties
RESULTS_FILE="$RESULTS_FILE$index_folder$temp"
mkdir $RESULTS_FILE

bin/trec_terrier.sh -i

#run the topics, use BM25
echo Which model do you wanna use?
read model
bin/trec_terrier.sh -r -Dtrec.model=$model


if [[($model == "BM25")]]
then
  run="/BM25b0.75_0.res"
fi

if [[($model == "TF_IDF")]]
then
  run="/TF_IDF_0.res"
fi

RESULTS_FILE="$RESULTS_FILE$run"


temp="_ALL.txt"
RESULTS_FNAME_ALL="$RESULTS_FNAME$index_folder$temp"
temp="_MAP.txt"
RESULTS_FNAME_MAP="$RESULTS_FNAME$index_folder$temp"
temp="_RPREC.txt"
RESULTS_FNAME_RPREC="$RESULTS_FNAME$index_folder$temp"
temp="_PAT10.txt"
RESULTS_FNAME_PAT10="$RESULTS_FNAME$index_folder$temp"

#EVALUATE THE RESULTS USING THE C LIBRARY OF TREC
$TREC_EVAL_FOLDER/trec_eval.sh -q -m all_trec $POOL_FILE $RESULTS_FILE >> $RESULTS_FNAME_ALL
$TREC_EVAL_FOLDER/trec_eval.sh -q -m map $POOL_FILE $RESULTS_FILE >> $RESULTS_FNAME_MAP
$TREC_EVAL_FOLDER/trec_eval.sh -q -m Rprec $POOL_FILE $RESULTS_FILE >> $RESULTS_FNAME_RPREC
$TREC_EVAL_FOLDER/trec_eval.sh -q -m P.10  $POOL_FILE $RESULTS_FILE >> $RESULTS_FNAME_PAT10
